import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  noClicked = false;
  goodChoiceHidden = true;

  constructor(public navCtrl: NavController) {

  }

  yes() {
    this.goodChoiceHidden = false;
  }
}
